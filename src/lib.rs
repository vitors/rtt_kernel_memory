#![no_std]
#![feature(alloc_error_handler)]

#[alloc_error_handler]
fn foo(_: core::alloc::Layout) -> ! {
    loop {}
}

extern crate slab_allocator_rs;

use slab_allocator_rs::LockedHeap;

#[global_allocator]
static ALLOCATOR: LockedHeap = LockedHeap::empty();

pub fn memory_init(start: usize, size: usize) {
    unsafe { ALLOCATOR.init(start, size) };
}
